from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
import sqlalchemy as DB


Base = declarative_base()


class Score(Base):
    __tablename__ = 'Scores'

    JoueurID = Column(Integer, primary_key=True)
    JeuID = Column(Integer)
    ValScore = Column(Integer)

    Pseudo = relationship("Joueurs", join="Scores.JoueurID == Joueurs.JoueurID")

    #billing_Joueurs_id = Column(Integer, ForeignKey("Joueurs.JoueurID"))
    #shipping_Joueurs_id = Column(Integer, ForeignKey("Joueurs.JoueurID"))
    #billing_Joueurs = relationship("Joueurs",foreign_keys=[billing_Joueurs_id])
    #shipping_Joueurs = relationship("Joueurs",foreign_keys=[shipping_Joueurs_id])
#
    def __repr__(self):
        return f"{self.JoueurID} : {self.ValScore} pts"

    def scores_joueurs(self):
        return f"{self.shipping_Joueurs.Pseudo} : {self.ValScore} points"

class Joueurs(Base):
    __tablename__ = "Joueurs"

    JoueurID = Column(Integer, primary_key=True)
    Pseudo = Column(String)
    Nom = Column(String)
    Prenon = Column(String)
    Adresse = Column(String)
    CodePostal = Column(Integer)
    Ville = Column(String)





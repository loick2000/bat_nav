import random

import settings
from Models import Bateau

class grille:
    """  sur ma_grille :
        0 = rien
        1 = inutile
        2 = bateau positionné de deux cases non touché
        20 = bateau positionné de deux cases touché
        3 = bateau positionné de trois cases non touché
        30 = bateau positionné de trois cases touché
        4 = bateau positionné de quatre cases non touché
        40 = bateau positionné de quatre cases touché
        5 = bateau positionné de cinq cases non touché
        50 = bateau positionné de cinq cases touché
        sur grille_adverse
        0 = rien
        1 = joué dans le vide
        2 = bateau touché
        3 = bateau coulé
    """
    def __init__(self, size):
        self.size = size
        self.ma_grille = []
        self.grille_adverse =[]
        self.bateaux = []
        for i in range(size):
            line =[]
            for j in range(size):
                line.append(0)
            self.ma_grille.append(line)
            self.grille_adverse.append(list(line))
        self.bateaux.append(Bateau.bateau(0,0,0,0))
        self.bateaux.append(Bateau.bateau(0,0,0,0))
        for nb_bateau in range(4):
            longueur = nb_bateau + 2
            # sens du bateau sur la grille 0 = horizontal, 1 = vertical
            sens = random.randint(0,1)
            if sens == 0: # donc en verticcal
                colonne_debut = random.randint(0,self.size - 1)
                ligne_debut = random.randint(0,self.size - longueur)
                # on vérifie que les cases ne sont pas occupées
                occupe = False
                while not occupe:
                    for i in range(longueur):
                        if self.ma_grille[colonne_debut][ligne_debut + i] > 0:
                            occupe = True
                    if not occupe:
                        for i in range(longueur):
                            self.ma_grille[colonne_debut][ligne_debut + i] = longueur
                            occupe = True
                        # la place n'est pas prise, on l'indique sur la grille
                        self.bateaux.append(Bateau.bateau(colonne_debut,ligne_debut,sens,longueur))
                        # print("bateau vertical de", longueur, ", commençant en col ", colonne_debut, ligne_debut)
                    else:
                        occupe = False
                        colonne_debut = random.randint(0, self.size - 1)
                        ligne_debut = random.randint(0, self.size - longueur)
            else:
                ligne_debut = random.randint(0,self.size -1)
                colonne_debut = random.randint(0,self.size - longueur)
                occupe = False
                while not occupe:
                    for i in range(longueur):
                        if self.ma_grille[colonne_debut + i][ligne_debut] > 0:
                            occupe = True
                    if not occupe:
                        # la place n'est pas prise, on l'indique sur la grille
                        for i in range(longueur):
                            self.ma_grille[colonne_debut + i][ligne_debut] = longueur
                            occupe = True
                        self.bateaux.append(Bateau.bateau(colonne_debut,ligne_debut,sens,longueur))
                        # print("bateau horiz longeur", longueur, ", commençant en col ", colonne_debut, ligne_debut)
                    else:
                        occupe = False
                        ligne_debut = random.randint(0, settings.taille - 1)
                        colonne_debut = random.randint(0, settings.taille - longueur)
        print("nombre de bateaux", self.bateaux.__len__() )

    def affiche_ma_grille(self,reel):
        for i in range(self.size):
            print(str(i)+" ",end="")
            for j in range(self.size):
                if j == self.size - 1:
                    fin_ligne = "|\n"
                else:
                    fin_ligne = ""
                if reel:
                    print("|" + str(self.ma_grille[i][j]), end=fin_ligne)
                else:
                    print("|" + (" " if self.ma_grille[i][j] == 0 else "X"), end=fin_ligne)
        print("   0 1 2 3 4 5 6 7 8 9")

    def affiche_grille_adverse(self,reel):
        for i in range(self.size):
            print(str(i)+" ",end="")
            for j in range(self.size):
                if j == self.size - 1:
                    fin_ligne = "|\n"
                else:
                    fin_ligne = ""
                if reel:
                    print("|" + str(self.ma_grille[i][j]), end=fin_ligne)
                else:
                    print("|" + (" " if self.grille_adverse[i][j] == 0 else "X"), end=fin_ligne)
        print("   0 1 2 3 4 5 6 7 8 9")

    def verif_tir(self,H,V):
        #le joueur adverse à joué, si la case de ma-_grille est à 0, c'est dans l'eau
        if self.ma_grille[H][V] == 0:
            print("Dans l'eau !")
            return 0
        else:
           self.ma_grille[H][V] = self.verif_coule(H,V)

    # un bateau est présent sur la case, on vérifie si le bateau est touché (reponse 2)
    # ou coulé (réponse 3)
    def verif_coule(self,H,V):
        coule = False
        # on boucle pour vérifier les
        for i in range(2,5):
            if self.bateaux[i].ici(H,V):
                self.ma_grille[H][V] = self.bateaux[i].etat()

        return 22222222222





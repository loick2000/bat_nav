import socket

HOST = 'localhost'
PORT = 22222


def client_thread(conn):
    pass


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
    s.bind((HOST, PORT))
    s.listen(2)
    conn, addr = s.accept()

    with conn:
        print('Connected by', addr)
        while True:
            data = conn.recv(1024)
            if not data: break

            conn.sendall("vous etes bien connecté")

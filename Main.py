import os
import random
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import socket

from Models import Players
import argparse

from Models.DBB import BDD

if __name__ == "__main__":
    # mise eplace du -h
    parser = argparse.ArgumentParser(description="Petit jeu de bataille navale")
    parser.add_argument("-J", dest="Joueur", nargs=2, help="Nom des 2 joueurs (facultatif)")
    parser.add_argument("-s", action="store_true", help="liste des scores")
    parser.add_argument("-j", action="store_true", help="Liste des joueurs.")
    parser.add_argument("-n", action="store_true", help="gestion réseau.")
    parser.add_help
    args = parser.parse_args()

    # liason BDD
    # engine = DB.create_engine("mssql+pyodbc://DESKTOP-8UNBRU0/BDJoueurs?driver=SQL+Server+Native+Client+11.0")
    # engine = DB.create_engine("mssql+pyodbc://DESKTOP-8UNBRU0/BDJoueurs?driver=SQL+Server")
    # context = engine.connect()
    #TBjoueurs = DB.Table("Joueurs",DB.MetaData())
    bdd = BDD()
    if args.n:
        HOST = '10.2.8.24'  # The remote host
        PORT = 22222  # The same port as used by the server
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
            s.connect((HOST, PORT))
            s.sendall(b'close')
            data = s.recv(1024)
        print('Received', repr(data))
    elif args.j:
        bdd.list_score()
    elif args.s:
        bdd.list_joueurs()
    else:
        joueurs = []
        for i in range(2):
            if args.Joueur:
                nom = args.Joueur[i]
            else:
                print("Quelle est le nom du joueur",i+1,"?")
                nom = input()
            joueurs.append(Players.player(nom))

        print(joueurs[0].name ," va donc jouer contre ", joueurs[1].name)
        joueurs[0].grille.affiche_ma_grille(reel=True)
        print("-"*50)
        joueurs[0].grille.affiche_grille_adverse(False)
        print("-"*50)
        joueurs[1].grille.affiche_ma_grille(True)
        print("-"*50)
        joueurs[1].grille.affiche_grille_adverse(False)

        fin = [False,False]
        while ( fin[0] and fin[1] ):
            joueur_courant = 0
            jeu = input("tirez : ")
            # vérification
            fin[0] = True


    bdd.close()

